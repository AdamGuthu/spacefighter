
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(2);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	// Checking the delay seconds to start the enemy ships
	if (m_delaySeconds > 0)
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed();
		// Activating enemy ships 
		if (m_delaySeconds <= 0)
		{
			GameObject::Activate(); // Activating the game objects
		}
	}
	// checking if the enemy ship is active
	if (IsActive())
	{
		m_activationSeconds += pGameTime->GetTimeElapsed();
		// Checking to see if enemy ship is not on screen to deactivate
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}
	// Updating the ships on the game screen
	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}